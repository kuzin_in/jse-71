package ru.kuzin.tm.dto.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ApplicationAboutResponse extends AbstractResponse {

    private String email;

    private String name;

    private String gitBranch;

    private String gitCommitId;

    private String gitCommitterName;

    private String gitCommitterEmail;

    private String gitCommitMessage;

    private String gitCommitTime;

}