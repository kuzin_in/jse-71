package ru.kuzin.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.kuzin.tm.api.repository.ICommandRepository;
import ru.kuzin.tm.listener.AbstractListener;

import java.util.Collection;
import java.util.Map;
import java.util.TreeMap;

@Repository
public class CommandRepository implements ICommandRepository {

    @NotNull
    private final Map<String, AbstractListener> mapByArgument = new TreeMap<>();

    @NotNull
    private final Map<String, AbstractListener> mapByName = new TreeMap<>();

    @Override
    public void add(@Nullable final AbstractListener command) {
        if (command == null) return;
        @NotNull final String name = command.getName();
        if (!name.isEmpty()) mapByName.put(name, command);
        @Nullable final String argument = command.getArgument();
        if (argument != null && !argument.isEmpty()) mapByArgument.put(argument, command);
    }

    @Nullable
    @Override
    public AbstractListener getCommandByArgument(@Nullable final String argument) {
        if (argument == null || argument.isEmpty()) return null;
        return mapByArgument.get(argument);
    }

    @Nullable
    @Override
    public AbstractListener getCommandByName(@Nullable final String name) {
        if (name == null || name.isEmpty()) return null;
        return mapByName.get(name);
    }

    @NotNull
    @Override
    public Iterable<AbstractListener> getCommandsWithArgument() {
        return mapByArgument.values();
    }

    @NotNull
    @Override
    public Collection<AbstractListener> getTerminalCommands() {
        return mapByName.values();
    }

}