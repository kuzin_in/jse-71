<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<html>
<head>
    <title>TASK MANAGER</title>
</head>
<style>

    a {
        color: darkblue;
    }

    select {
        width: 200px;
    }

    input[type="text"] {
        width: 200px;
    }

    input[type="date"] {
        width: 200px;
    }

</style>
<body>
<table width="100%" height="70%" border="1" style="">
    <tr>
        <td height="35" width="200" nowrap="nowrap" align="center">
            <b>TASK MANAGER</b>
        </td>
        <td width="100%" align="right">
            <a href="/projects">PROJECTS</a>
            |
            <a href="/tasks"> TASKS</a>
            <sec:authorize access="!isAuthenticated()">
                |
                <a href="/login">LOGIN</a>
            </sec:authorize>
            <sec:authorize access="isAuthenticated()">
                |
                USER: <sec:authentication property="name"/>
            </sec:authorize>
            <sec:authorize access="isAuthenticated()">
                |
                <a href="/logout">LOGOUT</a>
            </sec:authorize>

        </td>
    </tr>
    <tr>
        <td colspan="2" height="50%" valign="top" style="">
        <h1>WELCOME TO TASK-MANAGER</h1>
        </td>
    </tr>
</table>
</body>
