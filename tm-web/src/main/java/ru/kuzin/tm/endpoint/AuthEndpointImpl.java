package ru.kuzin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import ru.kuzin.tm.api.endpoint.IAuthEndpoint;
import ru.kuzin.tm.dto.CredentialsDTO;
import ru.kuzin.tm.dto.Result;
import ru.kuzin.tm.dto.UserDTO;
import ru.kuzin.tm.repository.dto.IUserDtoRepository;

import javax.annotation.Resource;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@RestController
@RequestMapping("api/auth")
@WebService(endpointInterface = "ru.kuzin.tm.api.endpoint.IAuthEndpoint")
public class AuthEndpointImpl implements IAuthEndpoint {

    @Resource
    private AuthenticationManager authenticationManager;

    @Autowired
    private IUserDtoRepository userDTORepository;

    @Override
    @NotNull
    @PostMapping("/login")
    @WebMethod
    public Result login(
            @WebParam(name = "credentials", partName = "credentials")
            @NotNull @RequestBody CredentialsDTO credentials) {
        try {
            final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(credentials.getUsername(), credentials.getPassword());
            final Authentication authentication = authenticationManager.authenticate(token);
            SecurityContextHolder.getContext().setAuthentication(authentication);
            return new Result(authentication.isAuthenticated());
        } catch (final Exception exception) {
            return new Result(exception);
        }
    }

    @Override
    @Nullable
    @GetMapping
    @WebMethod
    public UserDTO profile() {
        final SecurityContext securityContext = SecurityContextHolder.getContext();
        final Authentication authentication = securityContext.getAuthentication();
        final String username = authentication.getName();
        return userDTORepository.findByLogin(username);
    }

    @Override
    @NotNull
    @PostMapping("/logout")
    @WebMethod
    public Result logout() {
        SecurityContextHolder.getContext().setAuthentication(null);
        return new Result();
    }

}