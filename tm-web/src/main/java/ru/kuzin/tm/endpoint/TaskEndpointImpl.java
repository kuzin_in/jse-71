package ru.kuzin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.kuzin.tm.api.endpoint.ITaskEndpoint;
import ru.kuzin.tm.api.service.dto.ITaskDtoService;
import ru.kuzin.tm.dto.TaskDTO;
import ru.kuzin.tm.util.UserUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@RestController
@RequestMapping("/api/task")
@WebService(endpointInterface = "ru.kuzin.tm.api.endpoint.ITaskEndpoint")
public class TaskEndpointImpl implements ITaskEndpoint {

    @NotNull
    @Autowired
    private ITaskDtoService taskService;

    @Override
    @Nullable
    @GetMapping("/{id}")
    @WebMethod
    public TaskDTO findOneById(
            @WebParam(name = "id", partName = "id")
            @NotNull @PathVariable("id") String id) {
        return taskService.findOneById(UserUtil.getUserId(), id);
    }

    @Override
    @PostMapping
    @WebMethod
    public void saveOne(
            @WebParam(name = "task", partName = "task")
            @NotNull @RequestBody TaskDTO task) {
        taskService.save(UserUtil.getUserId(), task);
    }

    @Override
    @PutMapping
    @WebMethod
    public void updateOne(
            @WebParam(name = "task", partName = "task")
            @NotNull @RequestBody TaskDTO task) {
        taskService.save(UserUtil.getUserId(), task);
    }

    @Override
    @DeleteMapping("/{id}")
    @WebMethod
    public void deleteOneById(
            @WebParam(name = "id", partName = "id")
            @NotNull @PathVariable("id") String id) {
        taskService.removeOneById(UserUtil.getUserId(), id);
    }

}