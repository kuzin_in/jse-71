package ru.kuzin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import ru.kuzin.tm.api.service.dto.ITaskDtoService;
import ru.kuzin.tm.dto.soap.*;
import ru.kuzin.tm.util.UserUtil;

@Endpoint
public class TaskCollectionSoapEndpoint {

    @NotNull
    public final static String LOCATION_URI = "/ws";

    @NotNull
    public final static String PORT_TYPE_NAME = "TaskCollectionSoapEndpointPort";

    @NotNull
    public final static String NAMESPACE = "http://tm.kuzin.ru/dto/soap";

    @NotNull
    @Autowired
    private ITaskDtoService taskDTOService;

    @Nullable
    @ResponsePayload
    @PayloadRoot(localPart = "tasksFindAllRequest", namespace = NAMESPACE)
    public TasksFindAllResponse findCollection(
            @RequestPayload final TasksFindAllRequest request) {
        return new TasksFindAllResponse(taskDTOService.findAll(UserUtil.getUserId()));
    }

    @ResponsePayload
    @PayloadRoot(localPart = "tasksSaveRequest", namespace = NAMESPACE)
    public TasksSaveResponse saveCollection(
            @RequestPayload final TasksSaveRequest request) {
        taskDTOService.saveAll(UserUtil.getUserId(), request.getTasks());
        return new TasksSaveResponse();
    }

    @ResponsePayload
    @PayloadRoot(localPart = "tasksUpdateRequest", namespace = NAMESPACE)
    public TasksUpdateResponse updateCollection(
            @RequestPayload final TasksUpdateRequest request) {
        taskDTOService.saveAll(UserUtil.getUserId(), request.getTasks());
        return new TasksUpdateResponse();
    }

    @ResponsePayload
    @PayloadRoot(localPart = "tasksDeleteRequest", namespace = NAMESPACE)
    public TasksDeleteResponse deleteCollection(
            @RequestPayload final TasksDeleteRequest request) {
        taskDTOService.removeAll(UserUtil.getUserId(), request.getTasks());
        return new TasksDeleteResponse();
    }

}