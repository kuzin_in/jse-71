package ru.kuzin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.kuzin.tm.api.endpoint.IProjectCollectionEndpoint;
import ru.kuzin.tm.api.service.dto.IProjectDtoService;
import ru.kuzin.tm.api.service.model.IProjectService;
import ru.kuzin.tm.dto.ProjectDTO;
import ru.kuzin.tm.util.UserUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@RestController
@RequestMapping("/api/projects")
@WebService(endpointInterface = "ru.kuzin.tm.api.endpoint.IProjectCollectionEndpoint")
public class ProjectCollectionEndpointImpl implements IProjectCollectionEndpoint {

    @NotNull
    @Autowired
    private IProjectDtoService projectDTOService;

    @NotNull
    @Autowired
    private IProjectService projectService;

    @Override
    @Nullable
    @GetMapping
    @WebMethod
    public List<ProjectDTO> findAll() {
        return projectDTOService.findAll(UserUtil.getUserId());
    }

    @Override
    @PostMapping
    @WebMethod
    public void saveCollection(
            @WebParam(name = "projects", partName = "projects")
            @NotNull @RequestBody List<ProjectDTO> projects) {
        projectDTOService.saveAll(UserUtil.getUserId(), projects);
    }

    @Override
    @PutMapping
    @WebMethod
    public void updateCollection(
            @WebParam(name = "projects", partName = "projects")
            @NotNull @RequestBody List<ProjectDTO> projects) {
        projectDTOService.saveAll(UserUtil.getUserId(), projects);
    }

    @Override
    @DeleteMapping
    @WebMethod
    public void deleteCollection(
            @WebParam(name = "projects", partName = "projects")
            @NotNull @RequestBody List<ProjectDTO> projects) {
        projectDTOService.removeAll(UserUtil.getUserId(), projects);
    }

}