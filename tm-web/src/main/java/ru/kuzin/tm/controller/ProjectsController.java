package ru.kuzin.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.kuzin.tm.api.service.dto.IProjectDtoService;
import ru.kuzin.tm.model.CustomUser;

@Controller
public class ProjectsController {

    @NotNull
    @Autowired
    private IProjectDtoService projectService;

    @GetMapping("/projects")
    @PreAuthorize("hasAnyRole('ADMINISTRATOR', 'USER')")
    public ModelAndView index(@AuthenticationPrincipal final CustomUser user) {
        return new ModelAndView("project-list", "projects", projectService.findAll(user.getUserId()));
    }

}
