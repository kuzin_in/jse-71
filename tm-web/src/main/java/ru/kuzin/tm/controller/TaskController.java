package ru.kuzin.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.kuzin.tm.api.service.dto.IProjectDtoService;
import ru.kuzin.tm.api.service.dto.ITaskDtoService;
import ru.kuzin.tm.dto.ProjectDTO;
import ru.kuzin.tm.dto.TaskDTO;
import ru.kuzin.tm.enumerated.Status;
import ru.kuzin.tm.model.CustomUser;

import java.util.List;

@Controller
public class TaskController {

    @NotNull
    @Autowired
    private ITaskDtoService taskService;

    @Autowired
    private IProjectDtoService projectService;

    @GetMapping("/task/create")
    @PreAuthorize("hasAnyRole('ADMINISTRATOR', 'USER')")
    public String create(@AuthenticationPrincipal final CustomUser user) {
        @NotNull TaskDTO task = new TaskDTO("New Task" + System.currentTimeMillis());
        taskService.save(user.getUserId(), task);
        return "redirect:/tasks";
    }

    @GetMapping("/task/delete/{id}")
    @PreAuthorize("hasAnyRole('ADMINISTRATOR', 'USER')")
    public String delete(
            @AuthenticationPrincipal final CustomUser user,
            @PathVariable("id") String id
    ) {
        taskService.removeOneById(user.getUserId(), id);
        return "redirect:/tasks";
    }

    @PostMapping("/task/edit/{id}")
    @PreAuthorize("hasAnyRole('ADMINISTRATOR', 'USER')")
    public String edit(
            @AuthenticationPrincipal final CustomUser user,
            @ModelAttribute("task") TaskDTO task,
            BindingResult result
    ) {
        if (task.getProjectId().isEmpty()) task.setProjectId(null);
        System.out.println(task.getProjectId());
        taskService.save(user.getUserId(), task);
        return "redirect:/tasks";
    }

    @GetMapping("/task/edit/{id}")
    @PreAuthorize("hasAnyRole('ADMINISTRATOR', 'USER')")
    public ModelAndView edit(
            @AuthenticationPrincipal final CustomUser user,
            @PathVariable("id") String id) {
        final TaskDTO task = taskService.findOneById(user.getUserId(), id);
        final List<ProjectDTO> projects = projectService.findAll(user.getUserId());
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("task-edit");
        modelAndView.addObject("task", task);
        modelAndView.addObject("projects", projects);
        modelAndView.addObject("statuses", Status.values());
        return modelAndView;
    }

}