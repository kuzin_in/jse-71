package ru.kuzin.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.kuzin.tm.dto.ProjectDTO;

import java.util.List;

@Repository
public interface IProjectDtoRepository extends JpaRepository<ProjectDTO, String> {

    void deleteAllByUserId(@NotNull String userId);

    void deleteByIdAndUserId(@NotNull String id, @NotNull String userId);

    @NotNull
    List<ProjectDTO> findAllByUserId(@NotNull String userId);

    @Nullable
    ProjectDTO findByIdAndUserId(@NotNull String id, @NotNull String userId);

}