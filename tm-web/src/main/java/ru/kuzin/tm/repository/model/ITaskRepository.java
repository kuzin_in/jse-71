package ru.kuzin.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.kuzin.tm.model.Task;

import java.util.List;

@Repository
public interface ITaskRepository extends JpaRepository<Task, String> {

    @Modifying
    @Query("DELETE FROM #{#entityName} e WHERE e.user.id = :userId")
    void deleteAllByUserId(@NotNull @Param("userId") String userId);

    @Modifying
    @Query("DELETE FROM #{#entityName} e WHERE e.id = :id and e.user.id = :userId")
    void deleteByIdAndUserId(@NotNull @Param("id") String id, @Param("userId") @NotNull String userId);

    @NotNull
    @Query("SELECT e FROM #{#entityName} e WHERE e.user.id = :userId")
    List<Task> findAllByUserId(@NotNull @Param("userId") String userId);

    @Nullable
    @Query("SELECT e FROM #{#entityName} e WHERE e.id = :id and e.user.id = :userId")
    Task findByIdAndUserId(@NotNull @Param("id") String id, @NotNull @Param("userId") String userId);

}