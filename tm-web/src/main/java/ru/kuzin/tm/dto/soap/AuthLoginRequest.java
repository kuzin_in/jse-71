package ru.kuzin.tm.dto.soap;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.xml.bind.annotation.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@XmlType(name = "")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "authLoginRequest")
public class AuthLoginRequest {

    @XmlElement(required = true)
    protected String username;

    @XmlElement(required = true)
    protected String password;

}