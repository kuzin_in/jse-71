package ru.kuzin.tm.dto.soap;

import javax.xml.bind.annotation.XmlRegistry;

@XmlRegistry
public class ObjectFactory {

    public ObjectFactory() {
    }

    public AuthProfileRequest createAuthProfileRequest() {
        return new AuthProfileRequest();
    }

    public AuthProfileResponse createAuthProfileResponse() {
        return new AuthProfileResponse();
    }

    public AuthLoginRequest createAuthLoginRequest() {
        return new AuthLoginRequest();
    }

    public AuthLoginResponse createAuthLoginResponse() {
        return new AuthLoginResponse();
    }

    public AuthLogoutRequest createAuthLogoutRequest() {
        return new AuthLogoutRequest();
    }

    public AuthLogoutResponse createAuthLogoutResponse() {
        return new AuthLogoutResponse();
    }

    public ProjectDeleteRequest createProjectDeleteRequest() {
        return new ProjectDeleteRequest();
    }

    public ProjectsDeleteRequest createProjectsDeleteRequest() {
        return new ProjectsDeleteRequest();
    }

    public ProjectFindByIdRequest createProjectFindByIdRequest() {
        return new ProjectFindByIdRequest();
    }

    public ProjectsFindAllRequest createProjectsFindAllRequest() {
        return new ProjectsFindAllRequest();
    }

    public ProjectUpdateRequest createProjectUpdateRequest() {
        return new ProjectUpdateRequest();
    }

    public ProjectsUpdateRequest createProjectsUpdateRequest() {
        return new ProjectsUpdateRequest();
    }

    public ProjectSaveRequest createProjectSaveRequest() {
        return new ProjectSaveRequest();
    }

    public ProjectsSaveRequest createProjectsSaveRequest() {
        return new ProjectsSaveRequest();
    }

    public ProjectDeleteResponse createProjectDeleteResponse() {
        return new ProjectDeleteResponse();
    }

    public ProjectsDeleteResponse createProjectsDeleteResponse() {
        return new ProjectsDeleteResponse();
    }

    public ProjectFindByIdResponse createProjectFindByIdResponse() {
        return new ProjectFindByIdResponse();
    }

    public ProjectsFindAllResponse createProjectsFindAllResponse() {
        return new ProjectsFindAllResponse();
    }

    public ProjectUpdateResponse createProjectUpdateResponse() {
        return new ProjectUpdateResponse();
    }

    public ProjectsUpdateResponse createProjectsUpdateResponse() {
        return new ProjectsUpdateResponse();
    }

    public ProjectSaveResponse createProjectSaveResponse() {
        return new ProjectSaveResponse();
    }

    public ProjectsSaveResponse createProjectsSaveResponse() {
        return new ProjectsSaveResponse();
    }

    public TaskDeleteRequest createTaskDeleteRequest() {
        return new TaskDeleteRequest();
    }

    public TasksDeleteRequest createTasksDeleteRequest() {
        return new TasksDeleteRequest();
    }

    public TaskFindByIdRequest createTaskFindByIdRequest() {
        return new TaskFindByIdRequest();
    }

    public TasksFindAllRequest createTasksFindAllRequest() {
        return new TasksFindAllRequest();
    }

    public TaskUpdateRequest createTaskUpdateRequest() {
        return new TaskUpdateRequest();
    }

    public TasksUpdateRequest createTasksUpdateRequest() {
        return new TasksUpdateRequest();
    }

    public TaskSaveRequest createTaskSaveRequest() {
        return new TaskSaveRequest();
    }

    public TasksSaveRequest createTasksSaveRequest() {
        return new TasksSaveRequest();
    }

    public TaskDeleteResponse createTaskDeleteResponse() {
        return new TaskDeleteResponse();
    }

    public TasksDeleteResponse createTasksDeleteResponse() {
        return new TasksDeleteResponse();
    }

    public TaskFindByIdResponse createTaskFindByIdResponse() {
        return new TaskFindByIdResponse();
    }

    public TasksFindAllResponse createTasksFindAllResponse() {
        return new TasksFindAllResponse();
    }

    public TaskUpdateResponse createTaskUpdateResponse() {
        return new TaskUpdateResponse();
    }

    public TasksUpdateResponse createTasksUpdateResponse() {
        return new TasksUpdateResponse();
    }

    public TaskSaveResponse createTaskSaveResponse() {
        return new TaskSaveResponse();
    }

    public TasksSaveResponse createTasksSaveResponse() {
        return new TasksSaveResponse();
    }

}