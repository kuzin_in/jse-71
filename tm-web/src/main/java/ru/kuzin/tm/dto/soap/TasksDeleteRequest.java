package ru.kuzin.tm.dto.soap;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.kuzin.tm.dto.TaskDTO;

import javax.xml.bind.annotation.*;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@XmlType(name = "")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "tasksDeleteRequest")
public class TasksDeleteRequest {

    @XmlElement(required = true)
    protected List<TaskDTO> tasks;

}