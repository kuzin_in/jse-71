package ru.kuzin.tm.dto.soap;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.kuzin.tm.dto.ProjectDTO;
import ru.kuzin.tm.model.Project;

import javax.xml.bind.annotation.*;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@XmlType(name = "")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "projectsDeleteRequest")
public class ProjectsDeleteRequest {

    @XmlElement(required = true)
    protected List<ProjectDTO> projects;

}