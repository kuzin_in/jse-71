package ru.kuzin.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.web.bind.annotation.*;
import ru.kuzin.tm.dto.ProjectDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface IProjectCollectionEndpoint {

    @Nullable
    @GetMapping
    @WebMethod
    List<ProjectDTO> findAll();

    @PostMapping
    @WebMethod
    void saveCollection(
            @WebParam(name = "projects", partName = "projects")
            @NotNull @RequestBody List<ProjectDTO> projects);

    @PutMapping
    @WebMethod
    void updateCollection(
            @WebParam(name = "projects", partName = "projects")
            @NotNull @RequestBody List<ProjectDTO> projects);

    @DeleteMapping
    @WebMethod
    void deleteCollection(
            @WebParam(name = "projects", partName = "projects")
            @NotNull @RequestBody List<ProjectDTO> projects);

}