package ru.kuzin.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kuzin.tm.model.Task;

import java.util.Collection;
import java.util.List;


public interface ITaskService {

    void save(@Nullable final String userId, @Nullable final Task task);

    void saveAll(@Nullable final String userId, @Nullable final Collection<Task> tasks);

    void removeAll(@Nullable final String userId);

    void removeAll(@Nullable final String userId, @Nullable Collection<Task> tasks);

    void removeOneById(@Nullable final String userId, @Nullable final String id);

    void removeOne(@Nullable final String userId, @Nullable final Task task);

    @NotNull
    List<Task> findAll(@Nullable final String userId);

    @Nullable
    Task findOneById(@Nullable final String userId, @Nullable final String id);

}