package ru.kuzin.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.web.bind.annotation.*;
import ru.kuzin.tm.dto.TaskDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface ITaskCollectionEndpoint {

    @Nullable
    @GetMapping
    @WebMethod
    List<TaskDTO> findAll();

    @PostMapping
    @WebMethod
    void saveCollection(
            @WebParam(name = "tasks", partName = "tasks")
            @NotNull @RequestBody List<TaskDTO> tasks);

    @PutMapping
    @WebMethod
    void updateCollection(
            @WebParam(name = "tasks", partName = "tasks")
            @NotNull @RequestBody List<TaskDTO> tasks);

    @DeleteMapping
    @WebMethod
    void deleteCollection(
            @WebParam(name = "tasks", partName = "tasks")
            @NotNull @RequestBody List<TaskDTO> tasks);

}