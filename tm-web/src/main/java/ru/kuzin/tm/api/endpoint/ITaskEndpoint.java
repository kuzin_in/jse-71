package ru.kuzin.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.web.bind.annotation.*;
import ru.kuzin.tm.dto.TaskDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface ITaskEndpoint {

    @Nullable
    @GetMapping("/{id}")
    @WebMethod
    TaskDTO findOneById(String id);

    @PostMapping
    @WebMethod
    void saveOne(
            @WebParam(name = "task", partName = "task")
            @NotNull @RequestBody TaskDTO task);

    @PutMapping
    @WebMethod
    void updateOne(
            @WebParam(name = "task", partName = "task")
            @NotNull @RequestBody TaskDTO task);

    @DeleteMapping("/{id}")
    @WebMethod
    void deleteOneById(
            @WebParam(name = "id", partName = "id")
            @NotNull @PathVariable("id") String id);

}