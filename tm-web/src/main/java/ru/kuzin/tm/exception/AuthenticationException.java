package ru.kuzin.tm.exception;

public final class AuthenticationException extends AbstractException {

    public AuthenticationException() {
        super("Error! Authentication failed...");
    }

}
