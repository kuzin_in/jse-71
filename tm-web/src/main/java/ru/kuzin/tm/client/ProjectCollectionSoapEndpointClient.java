package ru.kuzin.tm.client;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.kuzin.tm.api.endpoint.IProjectCollectionEndpoint;

import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Service;
import java.net.URL;

public class ProjectCollectionSoapEndpointClient {

    @SneakyThrows
    public static IProjectCollectionEndpoint getInstance(@NotNull final String baseURL) {
        @NotNull final String wsdl = baseURL + "/jaxws/ProjectCollectionEndpointWS?wsdl";
        @NotNull final URL url = new URL(wsdl);
        @NotNull final String lp = "ProjectCollectionEndpointImplService";
        @NotNull final String ns = "http://endpoint.tm.kuzin.ru/";
        @NotNull final QName name = new QName(ns, lp);
        @NotNull final IProjectCollectionEndpoint result = Service.create(url, name).getPort(IProjectCollectionEndpoint.class);
        @NotNull final BindingProvider bindingProvider = (BindingProvider) result;
        bindingProvider.getRequestContext().put(BindingProvider.SESSION_MAINTAIN_PROPERTY, true);
        return result;
    }

}