package ru.kuzin.tm.unit.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.kuzin.tm.api.service.dto.ITaskDtoService;
import ru.kuzin.tm.config.ApplicationConfiguration;
import ru.kuzin.tm.dto.TaskDTO;
import ru.kuzin.tm.exception.AuthenticationException;
import ru.kuzin.tm.exception.IdEmptyException;
import ru.kuzin.tm.marker.UnitCategory;
import ru.kuzin.tm.util.UserUtil;

import java.util.Arrays;
import java.util.List;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ApplicationConfiguration.class})
@Category(UnitCategory.class)
public class TaskServiceTest {

    @NotNull
    @Autowired
    private ITaskDtoService taskDTOService;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @NotNull
    private final TaskDTO taskFirst = new TaskDTO("Task Test 1");

    @NotNull
    private final TaskDTO taskSecond = new TaskDTO("Task Test 2");

    @NotNull
    private final TaskDTO taskThird = new TaskDTO("Task Test 3");

    @NotNull
    private final TaskDTO taskFourth = new TaskDTO("Task Test 4");

    @Before
    public void initTest() {
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken("admin", "admin");
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        taskFirst.setUserId(UserUtil.getUserId());
        taskSecond.setUserId(UserUtil.getUserId());
        taskThird.setUserId(UserUtil.getUserId());
        taskDTOService.save(UserUtil.getUserId(), taskFirst);
        taskDTOService.save(UserUtil.getUserId(), taskSecond);
    }

    @After
    public void clear() {
        taskDTOService.removeAll(UserUtil.getUserId());
    }

    @Test
    @SneakyThrows
    public void saveTestNegative() {
        Assert.assertThrows(AuthenticationException.class, () -> taskDTOService.save(null, taskFirst));
        Assert.assertThrows(IdEmptyException.class, () -> taskDTOService.save(UserUtil.getUserId(), null));
    }

    @Test
    @SneakyThrows
    public void saveAllTestNegative() {
        Assert.assertThrows(AuthenticationException.class, () -> taskDTOService.saveAll(null, Arrays.asList(taskFirst)));
        Assert.assertThrows(IdEmptyException.class, () -> taskDTOService.saveAll(UserUtil.getUserId(), null));
    }

    @Test
    @SneakyThrows
    public void removeAllByUserIdTestNegative() {
        Assert.assertThrows(AuthenticationException.class, () -> taskDTOService.removeAll(null));
    }

    @Test
    @SneakyThrows
    public void removeCollectionTestNegative() {
        Assert.assertThrows(AuthenticationException.class, () -> taskDTOService.removeAll(null, Arrays.asList(taskFirst)));
    }

    @Test
    @SneakyThrows
    public void removeOneByIdTestNegative() {
        Assert.assertThrows(AuthenticationException.class, () -> taskDTOService.removeOneById(null, taskFirst.getId()));
        Assert.assertThrows(IdEmptyException.class, () -> taskDTOService.removeOneById(UserUtil.getUserId(), null));
        Assert.assertThrows(IdEmptyException.class, () -> taskDTOService.removeOneById(UserUtil.getUserId(), ""));
    }

    @Test
    @SneakyThrows
    public void removeOneTestNegative() {
        Assert.assertThrows(AuthenticationException.class, () -> taskDTOService.removeOne(null, taskFirst));
        Assert.assertThrows(IdEmptyException.class, () -> taskDTOService.removeOne(UserUtil.getUserId(), null));
    }

    @Test
    @SneakyThrows
    public void findAllTestNegative() {
        Assert.assertThrows(AuthenticationException.class, () -> taskDTOService.findAll(null));
    }

    @Test
    @SneakyThrows
    public void findOneByIdTestNegative() {
        Assert.assertThrows(AuthenticationException.class, () -> taskDTOService.findOneById(null, taskFirst.getId()));
        Assert.assertThrows(IdEmptyException.class, () -> taskDTOService.findOneById(UserUtil.getUserId(), null));
        Assert.assertThrows(IdEmptyException.class, () -> taskDTOService.findOneById(UserUtil.getUserId(), ""));
    }

    @Test
    @SneakyThrows
    public void saveTestPositive() {
        Assert.assertEquals(2, taskDTOService.findAll(UserUtil.getUserId()).size());
        taskDTOService.save(UserUtil.getUserId(), taskThird);
        List<TaskDTO> tasks = taskDTOService.findAll(UserUtil.getUserId());
        Assert.assertEquals(3, taskDTOService.findAll(UserUtil.getUserId()).size());
        Assert.assertNotNull(
                tasks.stream()
                        .filter(m -> taskThird.getId().equals(m.getId()))
                        .findFirst()
                        .orElse(null)
        );
    }

    @Test
    @SneakyThrows
    public void saveAllTestPositive() {
        Assert.assertEquals(2, taskDTOService.findAll(UserUtil.getUserId()).size());
        taskDTOService.saveAll(UserUtil.getUserId(), Arrays.asList(taskThird, taskFourth));
        Assert.assertEquals(4, taskDTOService.findAll(UserUtil.getUserId()).size());
    }

    @Test
    @SneakyThrows
    public void removeAllByUserIdTestPositive() {
        Assert.assertEquals(2, taskDTOService.findAll(UserUtil.getUserId()).size());
        taskDTOService.removeAll(UserUtil.getUserId());
        Assert.assertEquals(0, taskDTOService.findAll(UserUtil.getUserId()).size());
    }

    @Test
    @SneakyThrows
    public void removeCollectionTestPositive() {
        Assert.assertEquals(2, taskDTOService.findAll(UserUtil.getUserId()).size());
        taskDTOService.removeAll(UserUtil.getUserId(), Arrays.asList(taskFirst, taskSecond));
        Assert.assertEquals(0, taskDTOService.findAll(UserUtil.getUserId()).size());
    }

    @Test
    @SneakyThrows
    public void removeOneByIdTestPositive() {
        Assert.assertNotNull(
                taskDTOService.findAll(UserUtil.getUserId()).stream()
                        .filter(m -> taskFirst.getId().equals(m.getId()))
                        .findFirst()
                        .orElse(null)
        );
        taskDTOService.removeOneById(UserUtil.getUserId(), taskFirst.getId());
        Assert.assertNull(
                taskDTOService.findAll(UserUtil.getUserId()).stream()
                        .filter(m -> taskFirst.getId().equals(m.getId()))
                        .findFirst()
                        .orElse(null)
        );
    }

    @Test
    @SneakyThrows
    public void removeOneTestPositive() {
        Assert.assertNotNull(
                taskDTOService.findAll(UserUtil.getUserId()).stream()
                        .filter(m -> taskFirst.getId().equals(m.getId()))
                        .findFirst()
                        .orElse(null)
        );
        taskDTOService.removeOne(UserUtil.getUserId(), taskFirst);
        Assert.assertNull(
                taskDTOService.findAll(UserUtil.getUserId()).stream()
                        .filter(m -> taskFirst.getId().equals(m.getId()))
                        .findFirst()
                        .orElse(null)
        );
    }

    @Test
    @SneakyThrows
    public void findAllTestPositive() {
        Assert.assertEquals(2, taskDTOService.findAll(UserUtil.getUserId()).size());
        Assert.assertNotNull(
                taskDTOService.findAll(UserUtil.getUserId()).stream()
                        .filter(m -> taskFirst.getId().equals(m.getId()))
                        .findFirst()
                        .orElse(null)
        );
        Assert.assertNotNull(
                taskDTOService.findAll(UserUtil.getUserId()).stream()
                        .filter(m -> taskSecond.getId().equals(m.getId()))
                        .findFirst()
                        .orElse(null)
        );
    }

    @Test
    @SneakyThrows
    public void findOneByIdTestPositive() {
        Assert.assertEquals(taskFirst.getName(), taskDTOService.findOneById(UserUtil.getUserId(), taskFirst.getId()).getName());
    }

}