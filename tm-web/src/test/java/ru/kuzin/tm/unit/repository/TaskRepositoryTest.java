package ru.kuzin.tm.unit.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import ru.kuzin.tm.config.ApplicationConfiguration;
import ru.kuzin.tm.dto.TaskDTO;
import ru.kuzin.tm.marker.UnitCategory;
import ru.kuzin.tm.repository.dto.ITaskDtoRepository;
import ru.kuzin.tm.util.UserUtil;

import java.util.List;

@Transactional
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ApplicationConfiguration.class})
@Category(UnitCategory.class)
public class TaskRepositoryTest {

    @NotNull
    @Autowired
    private ITaskDtoRepository taskDTORepository;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @NotNull
    private final TaskDTO taskFirst = new TaskDTO("Task Test 1");

    @NotNull
    private final TaskDTO taskSecond = new TaskDTO("Task Test 2");

    @NotNull
    private final TaskDTO taskThird = new TaskDTO("Task Test 3");

    @Before
    public void initTest() {
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken("admin", "admin");
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        taskFirst.setUserId(UserUtil.getUserId());
        taskSecond.setUserId(UserUtil.getUserId());
        taskThird.setUserId(UserUtil.getUserId());
        taskDTORepository.save(taskFirst);
        taskDTORepository.save(taskSecond);
    }

    @After
    public void clear() {
        taskDTORepository.deleteAllByUserId(UserUtil.getUserId());
    }

    @Test
    @SneakyThrows
    public void findAllByUserIdTest() {
        List<TaskDTO> tasks = taskDTORepository.findAllByUserId(UserUtil.getUserId());
        Assert.assertEquals(2, tasks.size());
        for (final TaskDTO task : tasks) {
            Assert.assertNotNull(
                    tasks.stream()
                            .filter(m -> task.getId().equals(m.getId()))
                            .findFirst()
                            .orElse(null)
            );
        }
    }

    @Test
    @SneakyThrows
    public void findByIdAndUserIdTest() {
        Assert.assertEquals(taskFirst.getId(), taskDTORepository.findByIdAndUserId(taskFirst.getId(), UserUtil.getUserId()).getId());
    }

    @Test
    @SneakyThrows
    public void saveOneTest() {
        Assert.assertEquals(2, taskDTORepository.findAllByUserId(UserUtil.getUserId()).size());
        taskDTORepository.save(taskThird);
        List<TaskDTO> tasks = taskDTORepository.findAllByUserId(UserUtil.getUserId());
        Assert.assertEquals(3, taskDTORepository.findAllByUserId(UserUtil.getUserId()).size());
        Assert.assertNotNull(
                tasks.stream()
                        .filter(m -> taskThird.getId().equals(m.getId()))
                        .findFirst()
                        .orElse(null)
        );
    }

    @Test
    @SneakyThrows
    public void deleteAllByUserIdTest() {
        Assert.assertEquals(2, taskDTORepository.findAllByUserId(UserUtil.getUserId()).size());
        taskDTORepository.deleteAllByUserId(UserUtil.getUserId());
        Assert.assertEquals(0, taskDTORepository.findAllByUserId(UserUtil.getUserId()).size());
    }

    @Test
    @SneakyThrows
    public void deleteByIdAndUserIdTest() {
        Assert.assertEquals(2, taskDTORepository.findAllByUserId(UserUtil.getUserId()).size());
        Assert.assertNotNull(
                taskDTORepository.findAllByUserId(UserUtil.getUserId()).stream()
                        .filter(m -> taskFirst.getId().equals(m.getId()))
                        .findFirst()
                        .orElse(null)
        );
        taskDTORepository.deleteByIdAndUserId(taskFirst.getId(), UserUtil.getUserId());
        Assert.assertEquals(1, taskDTORepository.findAllByUserId(UserUtil.getUserId()).size());
        Assert.assertNull(
                taskDTORepository.findAllByUserId(UserUtil.getUserId()).stream()
                        .filter(m -> taskFirst.getId().equals(m.getId()))
                        .findFirst()
                        .orElse(null)
        );
    }

}