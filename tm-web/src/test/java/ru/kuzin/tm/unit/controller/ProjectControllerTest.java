package ru.kuzin.tm.unit.controller;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.kuzin.tm.api.service.dto.IProjectDtoService;
import ru.kuzin.tm.config.ApplicationConfiguration;
import ru.kuzin.tm.config.WebApplicationConfiguration;
import ru.kuzin.tm.dto.ProjectDTO;
import ru.kuzin.tm.marker.UnitCategory;
import ru.kuzin.tm.util.UserUtil;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {WebApplicationConfiguration.class, ApplicationConfiguration.class})
@Category(UnitCategory.class)
public class ProjectControllerTest {

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @NotNull
    private MockMvc mockMvc;

    @NotNull
    @Autowired
    private WebApplicationContext wac;

    @NotNull
    @Autowired
    private IProjectDtoService projectService;

    @NotNull
    private static final String PROJECTS_URL = "http://localhost:8080/projects";

    @NotNull
    private static final String PROJECT_URL = "http://localhost:8080/project";

    @NotNull
    private final ProjectDTO projectFirst = new ProjectDTO("Project Test 1");

    @NotNull
    private final ProjectDTO projectSecond = new ProjectDTO("Project Test 2");

    @NotNull
    private final ProjectDTO projectThird = new ProjectDTO("Project Test 3");

    @NotNull
    private final ProjectDTO projectFourth = new ProjectDTO("Project Test 4");

    @Before
    public void initTest() {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken("admin", "admin");
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        projectFirst.setUserId(UserUtil.getUserId());
        projectSecond.setUserId(UserUtil.getUserId());
        projectThird.setUserId(UserUtil.getUserId());
        projectFourth.setUserId(UserUtil.getUserId());
        projectService.save(UserUtil.getUserId(), projectFirst);
        projectService.save(UserUtil.getUserId(), projectSecond);
    }

    @After
    public void clear() {
        projectService.removeAll(UserUtil.getUserId());
    }

    @Test
    @SneakyThrows
    public void findAllTest() {
        mockMvc.perform(MockMvcRequestBuilders.get(PROJECTS_URL))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    @SneakyThrows
    public void createTest() {
        Assert.assertEquals(2, projectService.findAll(UserUtil.getUserId()).size());
        mockMvc.perform(MockMvcRequestBuilders.get(PROJECT_URL + "/create"))
                .andDo(print())
                .andExpect(status().is3xxRedirection());
        Assert.assertEquals(3, projectService.findAll(UserUtil.getUserId()).size());
    }

    @Test
    @SneakyThrows
    public void deleteTest() {
        Assert.assertEquals(2, projectService.findAll(UserUtil.getUserId()).size());
        mockMvc.perform(MockMvcRequestBuilders.get(PROJECT_URL + "/delete/" + projectFirst.getId()))
                .andDo(print())
                .andExpect(status().is3xxRedirection());
        Assert.assertEquals(1, projectService.findAll(UserUtil.getUserId()).size());
        Assert.assertNull(
                projectService.findAll(UserUtil.getUserId()).stream()
                        .filter(m -> projectFirst.getId().equals(m.getId()))
                        .findFirst()
                        .orElse(null)
        );
    }

    @Test
    @SneakyThrows
    public void editTest() {
        mockMvc.perform(MockMvcRequestBuilders.get(PROJECT_URL + "/edit/" + projectFirst.getId()))
                .andDo(print())
                .andExpect(status().is2xxSuccessful());
    }

}