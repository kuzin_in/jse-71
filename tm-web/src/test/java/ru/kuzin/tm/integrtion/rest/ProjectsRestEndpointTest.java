package ru.kuzin.tm.integrtion.rest;


import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;
import ru.kuzin.tm.dto.CredentialsDTO;
import ru.kuzin.tm.dto.ProjectDTO;
import ru.kuzin.tm.dto.Result;
import ru.kuzin.tm.marker.IntegrationCategory;

import java.net.HttpCookie;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;

@Category(IntegrationCategory.class)
public class ProjectsRestEndpointTest {

    @Nullable
    private static String sessionId;

    @NotNull
    private static final String PROJECTS_URL = "http://localhost:8080/api/projects";

    @NotNull
    private final ProjectDTO projectFirst = new ProjectDTO("Project Test 1");

    @NotNull
    private final ProjectDTO projectSecond = new ProjectDTO("Project Test 2");

    @NotNull
    private final ProjectDTO projectThird = new ProjectDTO("Project Test 3");

    @NotNull
    private final ProjectDTO projectFourth = new ProjectDTO("Project Test 4");

    @NotNull
    private static final HttpHeaders HEADER = new HttpHeaders();

    @BeforeClass
    public static void beforeClass() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final String url = "http://localhost:8080/api/auth/login";
        @NotNull CredentialsDTO credentials = new CredentialsDTO("admin", "admin");
        @NotNull final ResponseEntity<Result> response = restTemplate.postForEntity(url, new HttpEntity<CredentialsDTO>(credentials, HEADER), Result.class);
        Assert.assertEquals(200, response.getStatusCodeValue());
        Assert.assertNotNull(response.getBody());
        Assert.assertTrue(response.getBody().getSuccess());
        HttpHeaders headersResponse = response.getHeaders();
        List<HttpCookie> cookies = HttpCookie.parse(headersResponse.getFirst(HttpHeaders.SET_COOKIE));
        sessionId = cookies.stream()
                .filter(item -> "JSESSIONID".equals(item.getName()))
                .findFirst()
                .get()
                .getValue();
        Assert.assertNotNull(sessionId);
        HEADER.put(HttpHeaders.COOKIE, Arrays.asList("JSESSIONID=" + sessionId));
        HEADER.setContentType(MediaType.APPLICATION_JSON);
    }

    private static ResponseEntity<List> sendRequestList(@NotNull final String url, HttpMethod method, @NotNull final HttpEntity<List> httpEntity) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        return restTemplate.exchange(url, method, httpEntity, List.class);
    }

    private static ResponseEntity<ProjectDTO> sendRequest(@NotNull final String url, HttpMethod method, @NotNull final HttpEntity<List> httpEntity) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        return restTemplate.exchange(url, method, httpEntity, ProjectDTO.class);
    }

    @Before
    public void init() {
        sendRequestList(PROJECTS_URL, HttpMethod.POST, new HttpEntity<>(Arrays.asList(projectFirst, projectSecond), HEADER));
    }

    @After
    public void clear() {
        sendRequestList(PROJECTS_URL, HttpMethod.DELETE, new HttpEntity<>(sendRequestList(PROJECTS_URL, HttpMethod.GET, new HttpEntity<>(HEADER)).getBody(), HEADER));
    }

    @AfterClass
    public static void logout() {
        @NotNull final String url = "http://localhost:8080/api/auth/logout";
        sendRequest(url, HttpMethod.POST, new HttpEntity<>(HEADER));
    }

    @Test
    public void getTest() {
        Assert.assertEquals(2, sendRequestList(PROJECTS_URL, HttpMethod.GET, new HttpEntity<>(HEADER)).getBody().size());
    }

    @Test
    public void postTest() {
        Assert.assertEquals(2, sendRequestList(PROJECTS_URL, HttpMethod.GET, new HttpEntity<>(HEADER)).getBody().size());
        sendRequestList(PROJECTS_URL, HttpMethod.POST, new HttpEntity<>(Arrays.asList(projectThird, projectFourth), HEADER));
        List<ProjectDTO> list = sendRequestList(PROJECTS_URL, HttpMethod.GET, new HttpEntity<>(HEADER)).getBody();
        Assert.assertEquals(4, sendRequestList(PROJECTS_URL, HttpMethod.GET, new HttpEntity<>(HEADER)).getBody().size());
    }

    @Test
    public void putTest() {
        Assert.assertEquals(2, sendRequestList(PROJECTS_URL, HttpMethod.GET, new HttpEntity<>(HEADER)).getBody().size());
        projectFirst.setName("Name 1");
        projectSecond.setName("Name 2");
        sendRequestList(PROJECTS_URL, HttpMethod.POST, new HttpEntity<>(Arrays.asList(projectFirst, projectSecond), HEADER));
        Assert.assertEquals(2, sendRequestList(PROJECTS_URL, HttpMethod.GET, new HttpEntity<>(HEADER)).getBody().size());
        List<ProjectDTO> list = sendRequestList(PROJECTS_URL, HttpMethod.GET, new HttpEntity<>(HEADER)).getBody();
        Assert.assertEquals("Name 1", ((LinkedHashMap) sendRequestList(PROJECTS_URL, HttpMethod.GET, new HttpEntity<>(HEADER)).getBody().get(0)).get("name"));
        Assert.assertEquals("Name 2", ((LinkedHashMap) sendRequestList(PROJECTS_URL, HttpMethod.GET, new HttpEntity<>(HEADER)).getBody().get(1)).get("name"));
    }

    @Test
    public void deleteTest() {
        Assert.assertEquals(2, sendRequestList(PROJECTS_URL, HttpMethod.GET, new HttpEntity<>(HEADER)).getBody().size());
        sendRequestList(PROJECTS_URL, HttpMethod.DELETE, new HttpEntity<>(Arrays.asList(projectFirst, projectSecond), HEADER));
        Assert.assertEquals(0, sendRequestList(PROJECTS_URL, HttpMethod.GET, new HttpEntity<>(HEADER)).getBody().size());
    }

}
